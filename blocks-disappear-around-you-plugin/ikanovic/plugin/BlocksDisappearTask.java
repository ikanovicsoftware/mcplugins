package ikanovic.plugin;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class BlocksDisappearTask implements Runnable {
	private int radius;

	public BlocksDisappearTask(int radius) {
		this.radius = radius;
	}

	public void run() {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext()) {
			Player player = iterator.next();
			deleteRandomBlockAround(player.getLocation());
		}
	}

	private void deleteRandomBlockAround(Location location) {
		location.setX(location.getX() - radius);
		location.setY(location.getY() - radius);
		location.setZ(location.getZ() - radius);

		LinkedList<Block> blocks = getNonAirBlocks(location, radius);

		if (blocks.size() > 0) {
			removeRandomBlock(blocks);
		}

	}

	private LinkedList<Block> getNonAirBlocks(Location startLoc, int radius) {
		LinkedList<Block> blocks = new LinkedList<Block>();

		int startX = startLoc.getBlockX();
		int startY = startLoc.getBlockY();
		int startZ = startLoc.getBlockZ();
		Location tempLoc = startLoc;

		for (int x = startX; x < startX + (radius * 2); x++) {
			for (int y = startY; y < startY + (radius * 2); y++) {
				for (int z = startZ; z < startZ + (radius * 2); z++) {
					tempLoc.setX(x);
					tempLoc.setY(y);
					tempLoc.setZ(z);
					Block block = tempLoc.getBlock();

					if (isDeletableBlock(block)) {
						blocks.add(block);
					}
				}
			}
		}
		return blocks;
	}

	private boolean isDeletableBlock(Block block) {
		Material[] nonDeletableBlocks = {
			Material.AIR, 
			Material.CAVE_AIR, 
			Material.VOID_AIR,
			Material.BEDROCK,
			Material.OBSIDIAN,
			Material.NETHER_PORTAL,
			Material.END_GATEWAY,
			Material.END_PORTAL,
			Material.END_PORTAL_FRAME
		};
		
		for (int i = 0; i < nonDeletableBlocks.length; i++) {
			if (block.getType() == nonDeletableBlocks[i]) {
				return false;
			}
		}

		return true;
	}

	private void removeRandomBlock(LinkedList<Block> blocks) {
		Random rand = new Random();
		blocks.get(rand.nextInt(blocks.size())).setType(Material.AIR);
	}



}

