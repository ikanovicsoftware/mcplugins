package ikanovic.plugin;

import ikanovic.plugin.BlocksDisappearTask;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;

public class ReschedulerTask implements Runnable {

	private JavaPlugin plugin;
	private int startingDisappearTickRate = 200;
	private int currentDisappearTickRate = startingDisappearTickRate;

	/* 
	 * Valid tasks should never have ID of -1
	 * So we use -1 as a placeholder when no tasks are scheduled
   */ 
	private int blocksDisappearTaskID = -1;

	public ReschedulerTask(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	public void run() {
		if (blocksDisappearTaskID != -1) {
			Bukkit.getServer().getScheduler().cancelTask(blocksDisappearTaskID);
		}
		broadcastMessage("1 block disappears around you every " + (float) currentDisappearTickRate/20 + " seconds");
		blocksDisappearTaskID = scheduleBlocksDisappearingTask(currentDisappearTickRate);
		if (currentDisappearTickRate >= 5) {
			currentDisappearTickRate -= 5;
		}
	}

	public void cancelBlocksDisappearingTask() {
		if (blocksDisappearTaskID != -1) {
			Bukkit.getServer().getScheduler().cancelTask(blocksDisappearTaskID);
			blocksDisappearTaskID = -1;
		}
	}

	private int scheduleBlocksDisappearingTask(int ticks) {
		int radius = 5;
		return Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new BlocksDisappearTask(radius), ticks, ticks);
	}

	private void broadcastMessage(String message) {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext()) {
			Player currPlayer = iterator.next();
			currPlayer.sendMessage(message);
		}
	}
}

