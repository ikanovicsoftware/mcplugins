package ikanovic.plugin;

import ikanovic.plugin.ReschedulerTask;

// Java
import java.lang.Math;
import java.util.logging.Logger;
import java.util.Iterator;
import java.util.Collection;

// Bukkit
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.entity.*;

public class BlocksDisappearAroundYouPlugin extends JavaPlugin {
	Logger log = Logger.getLogger("Minecraft");

	/* 
	 * Valid tasks should never have ID of -1
	 * So we use -1 as a placeholder when no tasks are scheduled
   */ 
	private int reschedulerTaskID = -1;

	private ReschedulerTask reschedulerTask;
	private int reschedulerTickRate = 1200;

	private boolean isRunning = false;

	@Override
	public void onEnable() {
		log.info("Blocks Disappear Around You Plugin is enabled!");
	}

	@Override
	public void onDisable() {
		if (reschedulerTask != null) {
			reschedulerTask.cancelBlocksDisappearingTask();
		}
		if (reschedulerTaskID != -1) {
			Bukkit.getServer().getScheduler().cancelTask(reschedulerTaskID);
		}
		reschedulerTask = null;
		log.info("Blocks Disappear Around You Plugin is disabled!");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
    String[] args) {
		if (commandLabel.equals("blocksdisappeararoundyou")) {
			if (!isRunning) { // not already running
				isRunning = true;
				reschedulerTask = new ReschedulerTask(this);
				reschedulerTaskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, reschedulerTask, 0, reschedulerTickRate);
				broadcastMessage("Blocks no longer disappear around you!");
			}
			else { // already running
				isRunning = false;
				reschedulerTask.cancelBlocksDisappearingTask();
				Bukkit.getServer().getScheduler().cancelTask(reschedulerTaskID);
				reschedulerTask = null;
				broadcastMessage("Blocks disappear around you now!");
			}
		}
		return true;
	}

	private void broadcastMessage(String message) {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext()) {
			Player currPlayer = iterator.next();
			currPlayer.sendMessage(message);
		}
	}
}

