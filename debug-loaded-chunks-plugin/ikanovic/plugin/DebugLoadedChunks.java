package ikanovic.plugin;

// Java
import java.util.logging.Logger;
import java.util.List;

// Bukkit
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class DebugLoadedChunks extends JavaPlugin {
	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void onEnable() {
		log.info("Debug Load Chunks Plugin is enabled!");
	}

	@Override
	public void onDisable() {
		log.info("Debug Load Chunks Plugin is disabled!");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (commandLabel.equals("chunks")) {
			Player player = (Player) sender;
			List<World> worlds = Bukkit.getWorlds();
			for(int i = 0; i < worlds.size(); i++) {
				World world = worlds.get(i);
				log.info(world.getName() + " chunks: " + world.getLoadedChunks().length);
				player.sendMessage(world.getName() + " chunks: " + world.getLoadedChunks().length);
			}
		}
		return true;
	}
}

