# Plugin Ideas

_Check mark means completed_

- [ ] Minecraft but every random amount of time a 3x3 area under you turns to lava (Makes it scary to stop and mine)
- [ ] Minecraft but weeping angels but they look like friendly mobs
- [ ] Minecraft but all mobs in area target you and explode if they touch you
- [ ] Minecraft but blocks are deteriorating
- [ ] Minecraft but all zombies are giant zombies
