package ikanovic.plugin;

/* Bukkit */
import java.util.logging.Logger;
import org.bukkit.event.EventHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.block.Block;
import org.bukkit.Material;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.Listener;

/* Java */
import java.util.Iterator;
import java.util.Random;
import java.sql.Time;

public class LavaUnderneathYou extends JavaPlugin implements Listener {
	long lavaHappens = 0;
	long checkTime = 0;
	long startLava = 60000;
	boolean on = false;

	Logger log = Logger.getLogger("Minecraft");
	Random rand;

	@Override
	public void onEnable() {
		lavaHappens = System.currentTimeMillis();
		rand = new Random();

		log.info("'Lava Underneath You' Plugin enabled!");
		// Needed for Listener and EventHandling
		getServer().getPluginManager().registerEvents(this, this);
		int id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				spawnLavaUnderPlayer();
				;
			}
		}, 0, 2);
	}

	@Override
	public void onDisable() {
		log.info("'Lava Underneath You' Plugin disabled!");
	}

	// TODO: Change it so lava does not spawn if you are jumping over grass and
	// other various blocks. Lava spawns next solid block down

	private void spawnLavaUnderPlayer() {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();

		if (startLava + rand.nextInt(30000) < System.currentTimeMillis() - lavaHappens) {
			on = true;
		}

		while (iterator.hasNext() && on) {
			Player currPlayer = iterator.next();
			Location playerLocation = currPlayer.getLocation();
			playerLocation.setY(playerLocation.getY() - 1);
			Block block = playerLocation.getBlock();
			Material blockType = block.getType();

			if (blockType != Material.AIR && blockType != Material.CAVE_AIR && blockType != Material.VOID_AIR
					&& blockType != Material.GRASS && blockType != Material.LAVA) {
				log.info("Setting " + block.getType() + " to lava");
				block.setType(Material.LAVA);
			}

			lavaHappens = System.currentTimeMillis();
			on = false;
		}

	}
}
