package ikanovic.plugin;

// Java
import java.lang.Math;
import java.util.logging.Logger;
import java.util.Iterator;
import java.util.Collection;

// Bukkit
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.entity.*;

public class EverythingIsACreeperPlugin extends JavaPlugin {
	Logger log = Logger.getLogger("Minecraft");
	boolean isRunning = false;

	@Override
	public void onEnable() {
		log.info("Everything Is A Creeper Plugin is enabled!");
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				if (isRunning) {
					findAndExplodeMobsNearPlayers();
				}
			}
		}, 0, 2);
	}

	@Override
	public void onDisable() {
		log.info("Everything Is A Creeper Plugin is disabled!");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
    String[] args) {
		if (commandLabel.equals("everythingisacreeper")) {
			isRunning = !isRunning;
			if (isRunning) {
				broadcastMessage("All mobs are now creepers!");
			}
			else {
				broadcastMessage("Mobs are no longer creepers!");
			}
		}
		return true;
	}

	private void findAndExplodeMobsNearPlayers() {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext()) {
			Player currPlayer = iterator.next();
			Location chunkLocation = currPlayer.getLocation();
			findAndExplodeMobsInChunk(chunkLocation, currPlayer);
			chunkLocation.setX(chunkLocation.getX() + 16);
			findAndExplodeMobsInChunk(chunkLocation, currPlayer);
			chunkLocation = currPlayer.getLocation();
			chunkLocation.setX(chunkLocation.getX() - 16);
			findAndExplodeMobsInChunk(chunkLocation, currPlayer);
			chunkLocation = currPlayer.getLocation();
			chunkLocation.setZ(chunkLocation.getZ() + 16);
			findAndExplodeMobsInChunk(chunkLocation, currPlayer);
			chunkLocation = currPlayer.getLocation();
			chunkLocation.setZ(chunkLocation.getZ() - 16);
			findAndExplodeMobsInChunk(chunkLocation, currPlayer);

		}
	}

	private void findAndExplodeMobsInChunk(Location chunkLocation, Player player) {
		Chunk chunk = chunkLocation.getChunk();
		Location playerLocation = player.getLocation();
		Entity[] entities = chunk.getEntities();
		for(int i = 0; i < entities.length; i++) {
			if (entities[i] instanceof Mob && !entities[i].isDead()) {
				Location entityLocation = entities[i].getLocation();
				if (euclideanDistance(playerLocation, entityLocation) < 1.0) {
					player.sendMessage("Entity of type: " + entities[i].getType() + " exploded");
					explodeEntity(entities[i]);
				}
			}
		}
	}

	private double euclideanDistance(Location location1, Location location2) {
		double hypX = Math.pow((location1.getX() - location2.getX()), 2);
		double hypY = Math.pow((location1.getY() - location2.getY()), 2);
		double hypZ = Math.pow((location1.getZ() - location2.getZ()), 2);
		return Math.sqrt(hypX + hypY + hypZ);
	}

	private void explodeEntity(Entity entity) {
		entity.remove();
		Entity tntPrimed = entity.getWorld().spawn(entity.getLocation(), TNTPrimed.class);
		TNTPrimed explosion = (TNTPrimed) tntPrimed;
		explosion.setFuseTicks(15);
		explosion.setYield(2.5f);
	}

	private void broadcastMessage(String message) {
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext()) {
			Player currPlayer = iterator.next();
			currPlayer.sendMessage(message);
		}
	}

}

