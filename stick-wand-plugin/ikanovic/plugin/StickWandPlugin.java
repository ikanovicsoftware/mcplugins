package ikanovic.plugin;

// Java
import java.util.logging.Logger;

// Bukkit
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.*;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.*;

public class StickWandPlugin extends JavaPlugin implements Listener {
	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void onEnable() {
		log.info("Stick Wand Plugin is enabled!");
		// Needed for Listener and Event handlers
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {
		log.info("Stick Wand Plugin is disabled!");
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_AIR) {
			log.info("Player right clicked air");
			if(event.getItem().getType() == Material.STICK) {
				log.info("Player used stick");
				ItemStack item = new ItemStack(Material.SPLASH_POTION);
				PotionMeta meta = (PotionMeta) item.getItemMeta();
				meta.setColor(Color.RED);
				meta.addCustomEffect(new PotionEffect(PotionEffectType.HARM, 3, 1), true);
				item.setItemMeta(meta);
				ThrownPotion thrownPotion = event.getPlayer().launchProjectile(ThrownPotion.class);
				thrownPotion.setItem(item);
			}
		}
	}
}

