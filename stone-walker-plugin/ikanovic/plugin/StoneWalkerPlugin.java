package ikanovic.plugin;

// Java
import java.util.logging.Logger;

// Bukkit
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class StoneWalkerPlugin extends JavaPlugin implements Listener {
	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void onEnable() {
		log.info("Stone Walker Plugin enabled!");
		// Needed for Listener and EventHandling
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {
		log.info("Stone Walker Plugin disabled!");
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Location playerLoc = event.getPlayer().getLocation();
		playerLoc.setY(playerLoc.getY() - 1);
		Block block = playerLoc.getBlock();
		Material blockType = block.getType();
		if (blockType != Material.AIR && blockType != Material.CAVE_AIR && blockType != Material.VOID_AIR) {
			log.info("Setting " + block.getType() + " to stone");
			block.setType(Material.STONE);
		}
	}
}

