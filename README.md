# mcplugins

A library of Minecraft plugins to make the game overly difficult (and a few plugins that might be useful for debugging).

## Why create this?

We thought it would be fun to:
1. learn about ways to mod Minecraft
2. push the limits of Minecraft's difficulty and see if it's possible to beat the game

## Requirements to run plugins

* Java compiler/IDE
* Spigot/Bukkit
* God like PC
    * Why? Because the code is not optimized at all and probably uses all your RAM
* Expert level computer skills
    * Why? Because dragging and dropping .jar files into a folder takes a 4 year degree or higher

## Useful Resources

* [Beginner tutorial](https://www.spigotmc.org/wiki/spigot-plugin-development/)

* [Spigot's Javadoc](https://hub.spigotmc.org/javadocs/spigot/overview-summary.html)

## Developers

Quellus

Ami

_Use at your own discretion_
