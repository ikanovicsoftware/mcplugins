package ikanovic.plugin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Hello extends JavaPlugin {
	// Fired when plugin is first enabled
	@Override
	public void onEnable() {
		Bukkit.getServer().getLogger().info("'Hello World' is now enabled.");
	}

	// Fired when plugin is disabled
	@Override
	public void onDisable() {
		Bukkit.getServer().getLogger().info("'Hello World' is now disabled");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player) sender;

		player.sendMessage("Hello world!");

		return true;
	}
}